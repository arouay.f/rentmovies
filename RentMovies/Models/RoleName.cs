﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentMovies.Models
{
    public static class RoleName
    {
        public const string CanManageMovies = "CanManageMovies";
    }
}