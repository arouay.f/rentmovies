﻿using RentMovies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentMovies.ViewModels
{
    public class MovieFormViewModel
    {
        public Movie Movie { get; set; }
        public IEnumerable<Genre> Genres { get; set; }
        public String title
        {
            get
            {
                if (Movie != null && Movie.Id != 0)
                    return "Edit Movie";
                return "New Movie";
            }
        }
    }
}