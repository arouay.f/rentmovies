﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentMovies.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        
            [Required]
            [Display(Name = "Courrier électronique")]
            public string Email { get; set; }
            [Required]
            [StringLength(50)]
            public string Phone { get; set; }
        
    }
}