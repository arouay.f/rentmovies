﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RentMovies.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        
        public string Email { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Password {0} must contain at least {2} characters.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password ")]
        [Compare("Password", ErrorMessage = "Password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}