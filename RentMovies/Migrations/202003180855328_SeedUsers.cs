namespace RentMovies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8bafee7a-3357-40bc-89b0-7d16117215d3', N'admin@rentmovies.com', 0, N'AOSilmO+6zEiYv5NS+IQb9Lb9yPfSfTGfLDI07EB1imLsbolfysbIzyYkdIFt4iTGQ==', N'994539ea-1c7e-40ba-9949-a1732da9add3', NULL, 0, 0, NULL, 1, 0, N'admin@rentmovies.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e4c9b28c-592b-4877-a412-75ef854bae72', N'guest@rentmovies.com', 0, N'AOj2WvE3r/3Wmf2EEHJqKNWEBgqFCHnz9jDM9MKOuHekSoh1KfJexea3Ty3SNK9CUA==', N'7599a2d3-d92d-41a2-8e94-429531ec3462', NULL, 0, 0, NULL, 1, 0, N'guest@rentmovies.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'11ed6514-5563-4757-aa4f-f862c37f66b2', N'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8bafee7a-3357-40bc-89b0-7d16117215d3', N'11ed6514-5563-4757-aa4f-f862c37f66b2')

");
        }
        
        public override void Down()
        {
        }
    }
}
