namespace RentMovies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGenresTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Genres (Id, Name) VALUES (1, 'ACTION')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (2, 'THRILLER')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (3, 'FAMILY')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (4, 'ROMANCE')");
            Sql("INSERT INTO Genres (Id, Name) VALUES (5, 'COMEDY')");
        }
        
        public override void Down()
        {
        }
    }
}
