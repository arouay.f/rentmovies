﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RentMovies.Startup))]
namespace RentMovies
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
